package services;

import akka.actor.ActorSystem;
import scala.concurrent.ExecutionContext;

import javax.inject.Inject;

/**
 * @author adewaleafolabi
 */
public class AkkaExecutionContextManager implements ExecutionContextManager {
    @Inject
    ActorSystem actorSystem;
    @Override

    public ExecutionContext getExecutionContext() {
        return actorSystem.dispatchers().lookup("akka.db-dispatcher");
    }
}
