package services;

import scala.concurrent.ExecutionContext;

/**
 * @author adewaleafolabi
 */
public interface ExecutionContextManager {
    ExecutionContext getExecutionContext();
}
