package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * @author adewaleafolabi
 */
@Entity
public class Facility extends Model{
    @Id
    public long id;
    @Column(name = "facility_name")
    public String name;
    @ManyToOne
    public City city;
    public String address;
    @OneToMany
    public List<Receipt> receipts;

    public static Finder<Long, Facility> find = new Finder<>(Facility.class);


}
