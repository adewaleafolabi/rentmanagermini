package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by wale on 8/4/16.
 */
@Entity
public class Receipt extends Model{
    @Id
    public long id;
    @ManyToOne
    public Facility facility;
    @ManyToOne(cascade = CascadeType.ALL)
    public Visitor visitor;
    @Enumerated(EnumType.STRING)
    public PurposeOfStay purposeOfStay;
    public BigDecimal totalAmount;
    @OneToMany(cascade = CascadeType.ALL)
    public List<ReceiptItem> receiptItems;

    @Temporal(TemporalType.DATE)
    public Date createdDate = new Date();

    public static Finder<Long, Receipt> find = new Finder<>(Receipt.class);



}
