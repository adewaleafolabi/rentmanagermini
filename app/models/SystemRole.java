package models;

/**
 * @author adewaleafolabi
 */
public enum SystemRole {
    CREATE_USER,
    EDIT_USER,
    VIEW_USER,

    CREATE_RECEIPT,
    EDIT_RECEIPT,
    VIEW_RECEIPT,

    CREATE_SYSTEM_DATA,
    EDIT_SYSTEM_DATA,
    VIEW_SYSTEM_DATA,

    

}
