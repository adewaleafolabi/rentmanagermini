package models;

import com.avaje.ebean.Model;
import play.Logger;
import play.data.format.Formats;
import security.AppException;
import security.Hash;
import services.ExecutionContextManager;

import javax.inject.Inject;
import javax.persistence.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author adewaleafolabi
 */
@Entity
public class User extends Model{

    @Inject
    static ExecutionContextManager contextManager;

    @Id
    public long id;

    public String firstName;

    public String middleName;

    public String lastName;


    public String phoneNumber;

    public String staffNumber;

    @Column(unique = true)
    @Formats.NonEmpty
    public String username;

    @Formats.NonEmpty
    public String passwordHash;

    @Transient
    public String clearPassword;

    public boolean isEnabled = false;

    @ManyToMany
    public List<Role> roles;





    public static Finder<Long, User> find = new Finder<>(User.class);

    public  static CompletableFuture<List<User>> find(){

        return CompletableFuture.supplyAsync(() -> find.all());

    }

    public static User findByUserName(String username) {
        return find.where().eq("username",username).findUnique();
    }


    public static User authenticate(String username, String clearPassword) throws AppException {

        User user = find.where().eq("username", username).findUnique();

        if(user == null){

            Logger.debug("user not found");
        }
        System.out.println(Hash.createPassword("password123"));


        if (user != null) {

            Logger.debug("User found");

            if (Hash.checkPassword(clearPassword, user.passwordHash)) {

                return user;
            }

            Logger.error("Login authentication failed for user " + username);

        }

        return null;
    }
}
