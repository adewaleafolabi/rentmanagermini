package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by wale on 8/4/16.
 */
@Entity
@UniqueConstraint(columnNames = {"state_id","city_name"})
public class City extends Model {
    @Id
    public long id;
    @Column(name = "city_name")
    public String name;
    @ManyToOne
    public State state;
    @OneToMany
    public List<Facility> facilities;


    public static  Finder<Long,City> find = new Finder<>(City.class);

}
