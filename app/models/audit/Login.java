package models.audit;

import com.avaje.ebean.Model;
import models.User;
import play.data.format.Formats;

import javax.persistence.*;
import java.util.Calendar;

/**
 * Created by wale on 4/30/15.
 */
@Entity
@Table(name = "login_audit_trail")
public class Login  extends Model {
    @Id
    public long id;

    @Enumerated(EnumType.STRING)
    public LoginResult eventType;


    @Formats.DateTime(pattern="yyyy-MM-dd HH:mm:ss")
    @Column(updatable=false)
    public  java.sql.Timestamp  eventDate = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());

    public String ipAddress;

    public String username;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    public User user;

    public static Finder<Long, Login> find = new Finder<>(Login.class);


    public Login() {
    }

    public Login(LoginResult eventType, String ipAddress, String username) {
        this.eventType = eventType;
        this.ipAddress = ipAddress;
        this.username = username;
    }

    public Login(LoginResult eventType, String ipAddress, User user) {
        this.eventType = eventType;
        this.ipAddress = ipAddress;
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Login{");
        sb.append("id=").append(id);
        sb.append(", eventType=").append(eventType);
        sb.append(", eventDate=").append(eventDate);
        sb.append(", ipAddress='").append(ipAddress).append('\'');
        sb.append(", username='").append(username).append('\'');
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
