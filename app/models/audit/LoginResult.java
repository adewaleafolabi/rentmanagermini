package models.audit;

/**
 * Created by wale on 4/30/15.
 */
public enum LoginResult {
    LOGIN_OK,
    USER_NOT_FOUND,
    USER_NOT_ENABLED,
    USER_NOT_VERIFIED,
    LOGOUT_OK;


    @Override
    public String toString() {
        return name();
    }
}
