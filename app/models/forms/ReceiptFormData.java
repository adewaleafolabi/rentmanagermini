package models.forms;

import models.PurposeOfStay;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 8/13/16.
 */
public class ReceiptFormData {

    public long receiptId =0 ;
    public long visitorId = 0;
    public String visitorName;
    public String visitorAddress;
    public String visitorPhoneNumber;

    public long facilityId;

    public List<ReceiptItemFormData> receiptItems =new ArrayList<>();

    public PurposeOfStay purposeOfStay;
    public BigDecimal totalAmount;

    public static class ReceiptItemFormData{
        public long receiptItemID = 0;
        public double receiptItemQuantity;
        public long receiptItemServiceID;
    }
}
