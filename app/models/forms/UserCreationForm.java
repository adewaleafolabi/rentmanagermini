package models.forms;

import models.User;
import play.data.format.Formats;
import play.data.validation.Constraints;

import java.util.ArrayList;
import java.util.List;

/**
 * @author adewaleafolabi
 */
public class UserCreationForm {
    public long id=0;
    @Formats.NonEmpty
    @Constraints.Required
    public String username;
    public String password;
    public String firstName;
    public String middleName;
    public String lastName;
    public String staffNumber;
    public String phoneNumber;

    public List<Long> roles;

    public UserCreationForm() {
        roles= new ArrayList<>();
    }

    public String validate() {

        User user =  User.findByUserName(username);

        if(user !=null && id != user.id) {

            return "The username provided is already registered.";


        }

        return null;
    }
}
