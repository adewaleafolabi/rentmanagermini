package models.forms;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wale on 6/28/16.
 */
public class UserRoleUpdateForm {
    public long userID;
    public List<Long> roles;

    public UserRoleUpdateForm() {
        roles=new ArrayList<>();
    }
}
