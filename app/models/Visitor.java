package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by wale on 8/4/16.
 */
@Entity
public class Visitor extends Model{
    @Id
    public long id;
    public String name;
    public String address;
    public String phoneNumber;

    public static Finder<Long, Visitor> find = new Finder<>(Visitor.class);

}
