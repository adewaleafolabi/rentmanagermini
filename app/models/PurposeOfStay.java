package models;

/**
 * Created by wale on 8/4/16.
 */
public enum PurposeOfStay {
    Event, Academic, Leisure, Business, Other
}
