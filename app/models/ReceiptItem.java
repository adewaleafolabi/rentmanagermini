package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by wale on 8/4/16.
 */
@Entity
public class ReceiptItem extends Model {
    @Id
    public long id;
    public double quantity;
    @ManyToOne
    @Constraints.Required
    public Service service;

    public static Finder<Long, ReceiptItem> find = new Finder<>(ReceiptItem.class);



}
