package models;

import com.avaje.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

/**
 * @author adewaleafolabi
 */
@Entity
public class Role  extends Model{
    @Id
    public long id;
    @Enumerated(EnumType.STRING)
    public SystemRole roleName;
    public String roleDescription;

    public Role() {
    }

    public Role(long id) {
        this.id = id;
    }

    public static Finder<Long, Role> find = new Finder<>(Role.class);


}
