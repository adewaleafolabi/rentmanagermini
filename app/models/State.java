package models;

import com.avaje.ebean.Model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by wale on 8/4/16.
 */
@Entity
public class State extends Model{
    @Id
    public long id;
    @Column(name = "state_name",unique = true)
    public String name;

    @OneToMany
    public List<City> cities;

    public static  Finder<Long,State> find = new Finder<>(State.class);


}
