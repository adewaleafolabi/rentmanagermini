package models;

import com.avaje.ebean.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by wale on 8/4/16.
 */
@Entity
public class Service extends Model{
    @Id
    public long id;
    @Column(name = "service_name")
    public String name;

    public BigDecimal amount;

    @OneToMany
    public List<ReceiptItem> receiptItems;


    public static Finder<Long, Service> find = new Finder<>(Service.class);

}
