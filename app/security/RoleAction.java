package security;


import com.fasterxml.jackson.databind.node.ObjectNode;
import models.SystemRole;
import models.User;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

import static play.mvc.Controller.flash;

/**
 * Created by wale on 5/2/15.
 */
public class RoleAction extends Action<RoleSecured> {

    @Inject
    HostMaster hostMaster;

    @Override
    public CompletionStage<Result> call(Http.Context context) {

        SystemRole role = configuration.role();
        User user = hostMaster.getCurrentUser();

        boolean userHasRole = user.roles.stream().anyMatch(role1 -> role1.roleName.equals(role));

        if (false) {
            play.Logger.debug("Role requirement failed. Required profile - {} , User profile {}", role, user.roles.stream().map(role1 -> role1.roleName.name()).collect(Collectors.joining(",")));

            String message = "Your current profile does not permit you to carry out this task.";


            if (configuration.isJsonResponse()) {

                ObjectNode output = Json.newObject();

                output.put("message", message);

                return CompletableFuture.completedFuture(ok(output));

            }

            flash("warning", message);

            return CompletableFuture.completedFuture(
                    redirect(controllers.routes.DashboardController.index())
            );


        }
        return delegate.call(context);
    }


}
