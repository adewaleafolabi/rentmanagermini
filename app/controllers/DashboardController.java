package controllers;

import models.SystemRole;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;

/**
 * Created by wale on 8/5/16.
 */
public class DashboardController extends Controller {

    @Security.Authenticated(Secured.class)
    public Result index(){
        return ok(views.html.dashboard.render());
    }
}
