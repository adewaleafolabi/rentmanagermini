package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import models.Service;
import models.Service;
import models.State;
import models.SystemRole;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by wale on 8/6/16.
 */
public class ServicesController extends Controller {
    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result index() {
        return ok(views.html.service.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result create() {
        return ok(views.html.service.data.render(formFactory.form(Service.class)));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_SYSTEM_DATA)
    public Result edit(final long id) {

        return Optional.ofNullable(Service.find.byId(id))
                .map(service ->
                        ok(views.html.service.data.render(formFactory.form(Service.class).fill(service)))).orElseGet(() -> {
                    flash("error", "Service not found");

                    return redirect(routes.DashboardController.index());
                });


    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result save() {

        Form<Service> serviceForm = formFactory.form(Service.class).bindFromRequest();
        if (serviceForm.hasErrors() || serviceForm.hasGlobalErrors()) {
            serviceForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });
            return badRequest(views.html.service.data.render(serviceForm));
        }

        Service service = serviceForm.get();
        try {
            if (service.id != 0) {

                service.update();
            } else {
                service.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting service {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The service name is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.service.data.render(serviceForm));

        }

        flash("success", "Successful");

        return redirect(routes.ServicesController.index());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("");


        ExpressionList<Service> expressionList = Service.find.where();

        if(!query.isEmpty()){
            expressionList = expressionList.ilike("name",query+"%");

        }

        PagedList<Service> pagedList = expressionList.findPagedList(offset/perPage,perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", Service.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");

        pagedList.getList().stream().forEach(service -> {
            ObjectNode row = Json.newObject();

            row.put("name", service.name);
            row.put("amount", service.amount);
            row.put("action",service.id);

            an.add(row);
        });


        return ok(result);
    }

}
