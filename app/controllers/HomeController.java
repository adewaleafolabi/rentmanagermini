package controllers;

import models.User;
import models.audit.Login;
import models.audit.LoginResult;
import models.forms.LoginForm;
import play.Logger;
import play.cache.CacheApi;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.*;

import security.HostMaster;
import views.html.*;

import javax.inject.Inject;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    @Inject
    HostMaster hostMaster;

    @Inject
    CacheApi cache;

    @Inject
    FormFactory formFactory;

    public Result index() {
        return ok(login.render());
    }


    public Result login() {
        Form<LoginForm> loginForm = formFactory.form(LoginForm.class).bindFromRequest();

        if (loginForm.hasErrors()) {
            play.Logger.debug(loginForm.errorsAsJson().asText());
            String message =loginForm.errors().entrySet().stream().findFirst().map(s->s.getValue()).get().get(0).message();
            flash("error",message);
            return ok(views.html.login.render());
        }

        session("auth_user_name", loginForm.get().username);
        return redirect(controllers.routes.DashboardController.index());

    }


    public Result logout() {
        Logger.debug("Logout initiated");

        User currentUser = hostMaster.getCurrentUser();

        if (currentUser == null) {

            Logger.info("No user currently logged in");

        } else {

            cache.remove(currentUser.username + "_auth_user");

            session().clear();

            Logger.info("Cache and Session cleared");

            try {
                Login loginTrail = new Login(LoginResult.LOGOUT_OK, request().remoteAddress(), currentUser);

                loginTrail.save();

            } catch (Exception e) {

                play.Logger.error("failed to save audit trail {}", e);

            }

        }

        Logger.info("user ended session ");

        flash("success", "Logout Successful");


        return redirect(controllers.routes.HomeController.index());
    }

}
