package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import models.Facility;
import models.City;
import models.SystemRole;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by wale on 8/6/16.
 */
public class FacilityController extends Controller {
    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result index() {
        return ok(views.html.facility.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result create() {
        return ok(views.html.facility.data.render(formFactory.form(Facility.class), City.find.all()));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_SYSTEM_DATA)
    public Result edit(final long id) {

        return Optional.ofNullable(Facility.find.byId(id))
                .map(facility ->
                        ok(views.html.facility.data.render(formFactory.form(Facility.class).fill(facility),City.find.all()))).orElseGet(() -> {
                    flash("error", "Facility not found");

                    return redirect(routes.DashboardController.index());
                });


    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result save() {

        Form<Facility> facilityForm = formFactory.form(Facility.class).bindFromRequest();
        List<City> states = City.find.all();
        if (facilityForm.hasErrors() || facilityForm.hasGlobalErrors()) {
            facilityForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });
            return badRequest(views.html.facility.data.render(facilityForm,states));
        }

        Facility facility = facilityForm.get();
        try {
            if (facility.id != 0) {

                facility.update();
            } else {
                facility.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting facility {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The facility name is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.facility.data.render(facilityForm,states));

        }

        flash("success", "Successful");

        return redirect(routes.FacilityController.index());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("");


        ExpressionList<Facility> expressionList = Facility.find.where();

        if(!query.isEmpty()){
            expressionList = expressionList.ilike("name",query+"%");

        }

        PagedList<Facility> pagedList = expressionList.findPagedList(offset/perPage,perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", Facility.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");

        pagedList.getList().stream().forEach(facility -> {
            ObjectNode row = Json.newObject();

            row.put("name", facility.name);
            row.put("address", facility.address);
            row.put("city", facility.city.name);
            row.put("action",facility.id);

            an.add(row);
        });


        return ok(result);
    }

}
