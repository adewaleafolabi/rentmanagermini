package controllers;

import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import it.innove.play.pdf.PdfGenerator;
import models.*;
import models.forms.ReceiptFormData;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.HostMaster;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by wale on 8/6/16.
 */
public class ReceiptController extends Controller {
    @Inject
    FormFactory formFactory;

    @Inject
    HostMaster hostMaster;

    @Inject
    PdfGenerator pdfGenerator;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RECEIPT)
    public Result index() {
        return ok(views.html.receipt.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_RECEIPT)
    public Result create() {
        List<Service> services = Service.find.all();
        List<Facility> facilities = Facility.find.all();
        return ok(views.html.receipt.data.render(formFactory.form(ReceiptFormData.class), services, facilities));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RECEIPT)
    public Result print(final long id) {

        return Optional.ofNullable(Receipt.find.byId(id))
                .map(receipt -> ok(views.html.receipt.print.render(receipt,hostMaster.getCurrentUser()))).orElseGet(() -> {
                    flash("error", "Receipt not found");

                    return redirect(routes.ReceiptController.index());
                });
    }



    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RECEIPT)
    public Result pdf(final long id) {

        return Optional.ofNullable(Receipt.find.byId(id))
                .map(receipt -> ok(pdfGenerator.toBytes(views.html.receipt.print.render(receipt,hostMaster.getCurrentUser()),"http://localhost:9000"))).orElseGet(() -> {
                    flash("error", "Receipt not found");
                    return redirect(routes.ReceiptController.index());
                });
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_RECEIPT)
    public Result edit(final long id) {

        return Optional.ofNullable(Receipt.find.byId(id))
                .map(receipt ->
                        {
                            ReceiptFormData receiptFormData = new ReceiptFormData();
                            receiptFormData.receiptId = id;
                            receiptFormData.visitorId = receipt.visitor.id;
                            receiptFormData.visitorName = receipt.visitor.name;
                            receiptFormData.visitorAddress = receipt.visitor.address;
                            receiptFormData.visitorPhoneNumber = receipt.visitor.phoneNumber;
                            receiptFormData.facilityId = receipt.facility.id;
                            receiptFormData.purposeOfStay = receipt.purposeOfStay;
                            receiptFormData.totalAmount = receipt.totalAmount;
                            receiptFormData.receiptItems = receipt.receiptItems.stream().map(receiptItem -> {
                                ReceiptFormData.ReceiptItemFormData receiptItemFormData = new ReceiptFormData.ReceiptItemFormData();
                                receiptItemFormData.receiptItemID = receiptItem.id;
                                receiptItemFormData.receiptItemQuantity = receiptItem.quantity;
                                receiptItemFormData.receiptItemServiceID = receiptItem.service.id;

                                return receiptItemFormData;
                            }).collect(Collectors.toList());
                            return ok(views.html.receipt.data.render(formFactory.form(ReceiptFormData.class).fill(receiptFormData), Service.find.all(), Facility.find.all()));
                        }
                ).orElseGet(() -> {
                    flash("error", "Receipt not found");

                    return redirect(routes.DashboardController.index());
                });


    }
    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_RECEIPT)
    public Result save() {

        play.Logger.info("ReqBody:" + request().body().asFormUrlEncoded().keySet().toString());
        play.Logger.info("ReqBodyValues:" + request().body().asFormUrlEncoded().values().stream().map(f -> String.join(",", f)).collect(Collectors.joining(",")));


        List<Service> services = Service.find.all();
        List<Facility> facilities = Facility.find.all();

        Form<ReceiptFormData> receiptForm = formFactory.form(ReceiptFormData.class).bindFromRequest();
        if (receiptForm.hasErrors() || receiptForm.hasGlobalErrors()) {
            receiptForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });

            play.Logger.info("Receipt Errors: " + receiptForm.hasErrors());
            play.Logger.info("Receipt GErrors: " + receiptForm.hasGlobalErrors());
            return badRequest(views.html.receipt.data.render(receiptForm, services, facilities));

        }

        ReceiptFormData receiptFormData = receiptForm.get();

        if (receiptFormData.receiptItems != null && receiptFormData.receiptItems.size() > 0) {
            ReceiptFormData.ReceiptItemFormData receiptItem = receiptFormData.receiptItems.get(0);

            play.Logger.info(String.format("Qty:%s, Service:%s", receiptItem.receiptItemQuantity, receiptItem.receiptItemServiceID));

        }

        Receipt receipt = new Receipt();

        if (receiptFormData.receiptId > 0) {

            receipt = Receipt.find.byId(receiptFormData.receiptId);
        }

        receipt.visitor = new Visitor();
        if (receiptFormData.visitorId > 0) {
            receipt.visitor = Visitor.find.byId(receiptFormData.visitorId);
        }
        receipt.visitor.name = receiptFormData.visitorName;
        receipt.visitor.address = receiptFormData.visitorAddress;
        receipt.visitor.phoneNumber = receiptFormData.visitorPhoneNumber;
        receipt.facility = Facility.find.byId(receiptFormData.facilityId);
        receipt.purposeOfStay = receiptFormData.purposeOfStay;
        receipt.totalAmount = receiptFormData.totalAmount;


        receipt.receiptItems = receiptFormData.receiptItems.stream().map(receiptItemFormData -> {
            ReceiptItem receiptItem = new ReceiptItem();

            if (receiptItemFormData.receiptItemID > 0) {
                receiptItem = ReceiptItem.find.byId(receiptItemFormData.receiptItemID);

            }
            receiptItem.quantity = receiptItemFormData.receiptItemQuantity;
            receiptItem.service = Service.find.byId(receiptItemFormData.receiptItemServiceID);

            return receiptItem;
        }).collect(Collectors.toList());
        receipt.totalAmount = new BigDecimal(receipt.receiptItems.stream().mapToDouble(item -> (item.quantity * item.service.amount.doubleValue())).sum());

        try {
            if (receiptFormData.receiptId != 0) {

                receipt.update();
            } else {
                receipt.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting receipt {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The receipt name is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.receipt.data.render(receiptForm, services, facilities));
        }

        flash("success", "Successful");

        return redirect(routes.ReceiptController.print(receipt.id));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_RECEIPT)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("").trim();

        play.Logger.info("Query:{}" + query);
        ExpressionList<Receipt> expressionList = Receipt.find.where();

        if (!query.isEmpty()) {
            expressionList = expressionList.or(Expr.ilike("facility.name", query + "%"), Expr.ilike("visitor.name", query + "%"))
                    .or(Expr.ilike("visitor.address", query + "%"), Expr.ilike("totalAmount", query + "%"));


        }

        PagedList<Receipt> pagedList = expressionList.findPagedList(offset / perPage, perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", Facility.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");

        pagedList.getList().stream().forEach(receipt -> {
            ObjectNode row = Json.newObject();

            row.put("facilityName", receipt.facility.name);
            row.put("visitorName", receipt.visitor.name);
            row.put("visitorPhoneNumber", receipt.visitor.phoneNumber);
            row.put("visitorAddress", receipt.visitor.address);
            row.put("purpose", receipt.purposeOfStay.toString());
            row.put("totalAmount", receipt.totalAmount);
            row.put("action", receipt.id);

            an.add(row);
        });


        return ok(result);
    }

}
