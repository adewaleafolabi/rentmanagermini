package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import models.City;
import models.State;
import models.SystemRole;
import org.apache.commons.lang3.RandomStringUtils;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * Created by wale on 8/6/16.
 */
public class StateController extends Controller {
    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result index() {
        return ok(views.html.state.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result create() {
        return ok(views.html.state.data.render(formFactory.form(State.class)));
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_SYSTEM_DATA)
    public Result edit(final long id) {

        return Optional.ofNullable(State.find.byId(id))
                .map(state ->
                        ok(views.html.state.data.render(formFactory.form(State.class).fill(state)))).orElseGet(() -> {
                    flash("error", "State not found");

                    return redirect(routes.DashboardController.index());
                });


    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result save() {

        Form<State> stateForm = formFactory.form(State.class).bindFromRequest();

        if (stateForm.hasErrors() || stateForm.hasGlobalErrors()) {
            stateForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });
            return badRequest(views.html.state.data.render(stateForm));
        }

        State state = stateForm.get();
        try {
            if (state.id != 0) {

                state.update();
            } else {
                state.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting state {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The state name is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.state.data.render(stateForm));

        }

        flash("success", "Successful");

        return redirect(routes.StateController.index());
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("");


        ExpressionList<State> expressionList = State.find.where();

        if(!query.isEmpty()){
            expressionList = expressionList.ilike("name",query+"%");

        }

        PagedList<State> pagedList = expressionList.findPagedList(offset/perPage,perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", State.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");
        play.Logger.error("Size"+pagedList.getList().size());

        pagedList.getList().stream().forEach(state -> {
            ObjectNode row = Json.newObject();
            row.put("name", state.name);
            row.put("action",state.id);

            an.add(row);
        });


        return ok(result);
    }

}
