package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import models.City;

import models.State;
import models.SystemRole;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

/**
 * Created by wale on 8/6/16.
 */
public class CityController extends Controller {
    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result index() {
        return ok(views.html.city.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result create() {
        return ok(views.html.city.data.render(formFactory.form(City.class), State.find.all()));
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_SYSTEM_DATA)
    public Result edit(final long id) {

        return Optional.ofNullable(City.find.byId(id))
                .map(city ->
                        ok(views.html.city.data.render(formFactory.form(City.class).fill(city),State.find.all()))).orElseGet(() -> {
                    flash("error", "City not found");

                    return redirect(routes.DashboardController.index());
                });


    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_SYSTEM_DATA)
    public Result save() {

        Form<City> cityForm = formFactory.form(City.class).bindFromRequest();
        List<State> states = State.find.all();
        if (cityForm.hasErrors() || cityForm.hasGlobalErrors()) {
            cityForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });
            return badRequest(views.html.city.data.render(cityForm,states));
        }

        City city = cityForm.get();
        try {
            if (city.id != 0) {

                city.update();
            } else {
                city.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting city {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The city name is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.city.data.render(cityForm,states));

        }

        flash("success", "Successful");

        return redirect(routes.CityController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_SYSTEM_DATA)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("");


        ExpressionList<City> expressionList = City.find.where();

        if(!query.isEmpty()){
            expressionList = expressionList.ilike("name",query+"%");

        }

        PagedList<City> pagedList = expressionList.findPagedList(offset/perPage,perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", City.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");

        pagedList.getList().stream().forEach(city -> {
            ObjectNode row = Json.newObject();

            row.put("name", city.name);
            row.put("state", city.state.name);
            row.put("action",city.id);

            an.add(row);
        });


        return ok(result);
    }

}
