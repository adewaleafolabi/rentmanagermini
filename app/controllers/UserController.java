package controllers;

import com.avaje.ebean.ExpressionList;
import com.avaje.ebean.PagedList;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import models.*;
import models.forms.UserCreationForm;
import org.apache.commons.lang3.RandomStringUtils;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import security.Hash;
import security.RoleSecured;
import security.Secured;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by wale on 8/6/16.
 */
public class UserController extends Controller {
    @Inject
    FormFactory formFactory;

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result index() {
        return ok(views.html.user.index.render());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_USER)
    public Result create() {
        UserCreationForm userCreationForm = new UserCreationForm();
        userCreationForm.password = RandomStringUtils.randomAlphanumeric(8);

        Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).fill(userCreationForm);

        return ok(views.html.user.data.render(userForm, Role.find.all()));
    }


    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.EDIT_USER)
    public Result edit(final long id) {

        User user = User.find.byId(id);

        if (user == null) {
            flash("danger", "User not found");
            return redirect(controllers.routes.UserController.index());
        }

        List<Role> systemRoles = Role.find.all();


        UserCreationForm userCreationFormData = new UserCreationForm();
        userCreationFormData.username = user.username;
        userCreationFormData.roles = user.roles.stream().map(role -> role.id).collect(Collectors.toList());
        userCreationFormData.firstName = user.firstName;
        userCreationFormData.middleName = user.middleName;
        userCreationFormData.lastName = user.lastName;
        userCreationFormData.staffNumber = user.staffNumber;
        userCreationFormData.phoneNumber = user.phoneNumber;
        userCreationFormData.id = user.id;


        final Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).fill(userCreationFormData);

        return ok(views.html.user.data.render(userForm, systemRoles));

    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.CREATE_USER)
    public Result save() {
        boolean isUpdate = false;
        Form<UserCreationForm> userForm = formFactory.form(UserCreationForm.class).bindFromRequest();
        List<Role> systemRoles = Role.find.all();

        if (userForm.hasErrors() || userForm.hasGlobalErrors()) {
            userForm.errors().forEach((s, validationErrors) -> {
                System.out.println("Key:" + s);
                System.out.println(validationErrors.size());
            });
            return badRequest(views.html.user.data.render(userForm, systemRoles));
        }

        UserCreationForm userCreationData = userForm.get();

        User user;

        if (userCreationData.id > 0) {
            user = User.find.byId(userCreationData.id);
            isUpdate = true;
        } else {
            user = new User();
        }

        user.firstName = userCreationData.firstName;
        user.lastName = userCreationData.lastName;
        user.middleName = userCreationData.middleName;
        user.staffNumber = userCreationData.staffNumber;
        user.phoneNumber = userCreationData.phoneNumber;
        user.username = userCreationData.username;


        if (user.roles == null) {

            user.roles = new ArrayList<>();
        }

        user.roles.clear();
        userCreationData.roles.stream().forEach(i -> {
            user.roles.add(new Role(i));
        });

        try {
            if (isUpdate) {
                if (!Optional.ofNullable(userCreationData.password).orElse("").isEmpty()) {
                    user.passwordHash = Hash.createPassword(userCreationData.password);

                }
                user.update();
            } else {
                user.passwordHash = Hash.createPassword(userCreationData.password);
                user.save();
            }

        } catch (Exception e) {
            play.Logger.error("Error persisting city {]", e);
            String error = "Technical error. Please try again";

            if (e.getCause() instanceof MySQLIntegrityConstraintViolationException) {

                error = "The username is already in use";
            }

            flash("danger", error);
            return badRequest(views.html.user.data.render(userForm, systemRoles));


        }

        flash("success", "Successful");

        return redirect(routes.UserController.index());
    }

    @Security.Authenticated(Secured.class)
    @RoleSecured(role = SystemRole.VIEW_USER)
    public Result search() {

        int perPage = Optional.ofNullable(request().getQueryString("perPage")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int offset = Optional.ofNullable(request().getQueryString("offset")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        int page = Optional.ofNullable(request().getQueryString("page")).map(s -> {
            if (s.matches("\\d+")) {
                return Integer.valueOf(s);
            }
            return 0;
        }).get();

        String query = Optional.ofNullable(request().getQueryString("queries[search]")).orElse("");


        ExpressionList<User> expressionList = User.find.where();

        if (!query.isEmpty()) {
            expressionList = expressionList.ilike("username", query + "%");

        }

        PagedList<User> pagedList = expressionList.findPagedList(offset / perPage, perPage);


        ObjectNode result = Json.newObject();

        result.put("totalRecordCount", User.find.findRowCount());
        result.put("queryRecordCount", pagedList.getTotalRowCount());

        ArrayNode an = result.putArray("records");

        pagedList.getList().stream().forEach(user -> {
            ObjectNode row = Json.newObject();

            row.put("firstName", user.firstName);
            row.put("nickName", user.middleName);
            row.put("lastName", user.middleName);
            row.put("phoneNumber", user.phoneNumber);
            row.put("username", user.username);
            row.put("action", user.id);

            an.add(row);
        });


        return ok(result);
    }

}
