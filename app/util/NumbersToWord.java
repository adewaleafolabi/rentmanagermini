/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;


import com.ibm.icu.text.RuleBasedNumberFormat;
import java.math.BigDecimal;
import java.math.RoundingMode;
import org.apache.commons.lang3.text.WordUtils;


/**
 *
 * @author wale
 */
public class NumbersToWord {

    public static String convert(BigDecimal number) {

        number = number.setScale(2, RoundingMode.CEILING);

        String[] amountParts = number.toPlainString().split("\\.");

        RuleBasedNumberFormat formatter = new RuleBasedNumberFormat(RuleBasedNumberFormat.SPELLOUT);

        String amountPart1 = formatter.format(Double.valueOf(amountParts[0])).replace("hundred", "hundred and").replaceAll(" and$", "");
        String amountPart2 = " naira only";

        if (!amountParts[1].equals("00")) {
            amountPart2 = String.format(" naira %s kobo only", formatter.format(Double.valueOf(amountParts[1])));
        }

        return WordUtils.capitalizeFully(String.format("%s%s", amountPart1, amountPart2));

    }
}
