package vo;

/**
 * @author adewaleafolabi
 */
public class ServiceResponse {
    public int responseCode;
    public String message;

    public ServiceResponse() {
    }

    public ServiceResponse(int responseCode, String message) {
        this.responseCode = responseCode;
        this.message = message;
    }

    public static int SUCCESS = 0;
    public static int ERROR = 10;
    public static String USER_CREATION_OK = "";
    
    public static String BUILDING_CREATION_OK = "";
    public static String BUILDING_UPDATE_OK = "";
    public static String BUILDING_NOT_FOUND = "";

    public static String RESERVATION_CREATION_OK = "";
    public static String RESERVATION_UPDATE_OK = "";
    public static String RESERVATION_NOT_FOUND = "";

    public static String CUSTOMER_CREATION_OK = "";
    public static String CUSTOMER_UPDATE_OK = "";
    public static String CUSTOMER_NOT_FOUND = "";
    public static String FORM_HAS_ERRORS = "";
    public static String USER_CREATION_FAILED = "";
    public static String USER_NOT_FOUND = "User not found";
    public static String GENERAL_SUCCESS_MESSAGE = "";
    public static String USER_ROLE_UPDATE_OK = "User Role update successful";
    public static String GENERAL_FAILURE_MESSAGE = "Technical error. Please try again";


}
