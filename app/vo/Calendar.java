package vo;

/**
 * @author adewaleafolabi
 */
public class Calendar {
    public long id;
    public String title;
    public String start;
    public String end;

    public Calendar(long id, String title, String start, String end) {
        this.id = id;
        this.title = title;
        this.start = start;
        this.end = end;
    }
}
