# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table city (
  id                            bigint auto_increment not null,
  city_name                     varchar(255),
  state_id                      bigint,
  constraint uq_city_state_id_city_name unique (state_id,city_name),
  constraint pk_city primary key (id)
);

create table facility (
  id                            bigint auto_increment not null,
  facility_name                 varchar(255),
  city_id                       bigint,
  address                       varchar(255),
  constraint pk_facility primary key (id)
);

create table login_audit_trail (
  id                            bigint auto_increment not null,
  event_type                    varchar(17),
  event_date                    datetime(6),
  ip_address                    varchar(255),
  username                      varchar(255),
  user_id                       bigint,
  constraint ck_login_audit_trail_event_type check (event_type in ('LOGIN_OK','USER_NOT_FOUND','USER_NOT_ENABLED','USER_NOT_VERIFIED','LOGOUT_OK')),
  constraint pk_login_audit_trail primary key (id)
);

create table receipt (
  id                            bigint auto_increment not null,
  facility_id                   bigint,
  visitor_id                    bigint,
  purpose_of_stay               varchar(8),
  total_amount                  decimal(38),
  created_date                  date,
  constraint ck_receipt_purpose_of_stay check (purpose_of_stay in ('Event','Academic','Leisure','Business','Other')),
  constraint pk_receipt primary key (id)
);

create table receipt_item (
  id                            bigint auto_increment not null,
  receipt_id                    bigint not null,
  quantity                      double,
  service_id                    bigint,
  constraint pk_receipt_item primary key (id)
);

create table role (
  id                            bigint auto_increment not null,
  role_name                     varchar(18),
  role_description              varchar(255),
  constraint ck_role_role_name check (role_name in ('CREATE_USER','EDIT_USER','VIEW_USER','CREATE_RECEIPT','EDIT_RECEIPT','VIEW_RECEIPT','CREATE_SYSTEM_DATA','EDIT_SYSTEM_DATA','VIEW_SYSTEM_DATA')),
  constraint pk_role primary key (id)
);

create table service (
  id                            bigint auto_increment not null,
  service_name                  varchar(255),
  amount                        decimal(38),
  constraint pk_service primary key (id)
);

create table state (
  id                            bigint auto_increment not null,
  state_name                    varchar(255),
  constraint uq_state_state_name unique (state_name),
  constraint pk_state primary key (id)
);

create table user (
  id                            bigint auto_increment not null,
  first_name                    varchar(255),
  middle_name                   varchar(255),
  last_name                     varchar(255),
  phone_number                  varchar(255),
  staff_number                  varchar(255),
  username                      varchar(255),
  password_hash                 varchar(255),
  is_enabled                    tinyint(1) default 0,
  constraint uq_user_username unique (username),
  constraint pk_user primary key (id)
);

create table user_role (
  user_id                       bigint not null,
  role_id                       bigint not null,
  constraint pk_user_role primary key (user_id,role_id)
);

create table visitor (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  address                       varchar(255),
  phone_number                  varchar(255),
  constraint pk_visitor primary key (id)
);

alter table city add constraint fk_city_state_id foreign key (state_id) references state (id) on delete restrict on update restrict;
create index ix_city_state_id on city (state_id);

alter table facility add constraint fk_facility_city_id foreign key (city_id) references city (id) on delete restrict on update restrict;
create index ix_facility_city_id on facility (city_id);

alter table login_audit_trail add constraint fk_login_audit_trail_user_id foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_login_audit_trail_user_id on login_audit_trail (user_id);

alter table receipt add constraint fk_receipt_facility_id foreign key (facility_id) references facility (id) on delete restrict on update restrict;
create index ix_receipt_facility_id on receipt (facility_id);

alter table receipt add constraint fk_receipt_visitor_id foreign key (visitor_id) references visitor (id) on delete restrict on update restrict;
create index ix_receipt_visitor_id on receipt (visitor_id);

alter table receipt_item add constraint fk_receipt_item_receipt_id foreign key (receipt_id) references receipt (id) on delete restrict on update restrict;
create index ix_receipt_item_receipt_id on receipt_item (receipt_id);

alter table receipt_item add constraint fk_receipt_item_service_id foreign key (service_id) references service (id) on delete restrict on update restrict;
create index ix_receipt_item_service_id on receipt_item (service_id);

alter table user_role add constraint fk_user_role_user foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_user_role_user on user_role (user_id);

alter table user_role add constraint fk_user_role_role foreign key (role_id) references role (id) on delete restrict on update restrict;
create index ix_user_role_role on user_role (role_id);


# --- !Downs

alter table city drop foreign key fk_city_state_id;
drop index ix_city_state_id on city;

alter table facility drop foreign key fk_facility_city_id;
drop index ix_facility_city_id on facility;

alter table login_audit_trail drop foreign key fk_login_audit_trail_user_id;
drop index ix_login_audit_trail_user_id on login_audit_trail;

alter table receipt drop foreign key fk_receipt_facility_id;
drop index ix_receipt_facility_id on receipt;

alter table receipt drop foreign key fk_receipt_visitor_id;
drop index ix_receipt_visitor_id on receipt;

alter table receipt_item drop foreign key fk_receipt_item_receipt_id;
drop index ix_receipt_item_receipt_id on receipt_item;

alter table receipt_item drop foreign key fk_receipt_item_service_id;
drop index ix_receipt_item_service_id on receipt_item;

alter table user_role drop foreign key fk_user_role_user;
drop index ix_user_role_user on user_role;

alter table user_role drop foreign key fk_user_role_role;
drop index ix_user_role_role on user_role;

drop table if exists city;

drop table if exists facility;

drop table if exists login_audit_trail;

drop table if exists receipt;

drop table if exists receipt_item;

drop table if exists role;

drop table if exists service;

drop table if exists state;

drop table if exists user;

drop table if exists user_role;

drop table if exists visitor;

